use company;

show table status;

create temporary table products_below_avg
select Productid, Productname, Price
from Products
where Price < (select avg(Price) from Products);

drop table products_below_avg;

select * from products_below_avg;